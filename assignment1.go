/*
 *	Author: Tyler Arseneault
 *	ID: 	3052448
 *	Due: 	Feb 6, 2018
 *	Class:	CMPT 315
 *			Assignment 1
 */

package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

/*
 * A simple struct to handle the error code information specific for each situation
 */
type Info struct {
	Code        int    `json:"code" xml:"code"`
	Description string `json:"desc" xml:"desc"`
	Message     string `json:"message" xml:"message"`
}

var codes map[int]string
var logger *log.Logger

/*
 * Description: Initializes global variables
 */
func init() {
	//map containing error code information
	codes = map[int]string{
		200: "OK",
		201: "Created",
		204: "No Content",
		400: "Bad Request",
		403: "Forbidden",
		404: "Not Found",
	}

	//logger for logging any fatal errors to the stdout
	logger = log.New(os.Stdout, "Error: ", log.LstdFlags)
}

/*
 * Description: Generates a random string of a specified length to be used as
 * a secret token
 * Param: int	length of the token
 * Returns: string	token
 * Source: https://www.calhoun.io/creating-random-strings-in-go/
 */
func generateSecretToken(length int) string {

	//Characters to be picked randomly to be used in the generated string
	characters := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!?@"

	//set the seed to the current time for the randomly generated integer
	seed := rand.New(rand.NewSource(time.Now().UnixNano()))

	//create a slice of bytes of length 10
	//fill each space of the slice with a random character from the list
	secret := make([]byte, 10)
	for i := 0; i < len(secret); i++ {
		secret[i] = characters[seed.Intn(len(characters))]
	}

	//convert the slice of bytes to a string and return it
	return string(secret)

}

/*
 * Description: Handles the POST method on the class route
 * Param: w Writer	r http.Request
 */
func handlePostClass(w http.ResponseWriter, r *http.Request) {
	//to contain any log information to be displayed in json
	var information Info

	classroom_id := r.FormValue("classroom_id") //get the classroom id from the request body
	name := r.FormValue("name")                 //get the classroom name from the request body
	secret := generateSecretToken(10)           //generate the 10 digit secret token
	expires := time.Now().Add(time.Hour * 2)    //set the expiration of the cookie

	//create the cookie containing the secret token
	cookie := http.Cookie{Name: "secret_token", Value: secret, Expires: expires}

	//post the new class using the id and name chosen, and the secret token generated
	err := postClass(classroom_id, name, secret)
	if err != nil {
		//if class could not be posted, display the error
		information = Info{
			Code:        400,
			Description: codes[400],
			Message:     classroom_id + " could not be added",
		}
	} else {
		//if the class was posted, display the success message
		information = Info{
			Code:        201,
			Description: codes[201],
			Message:     classroom_id + " was successfully added",
		}

		//set the cookie for use
		http.SetCookie(w, &cookie)
	}

	//encode the information to be displayed, and then send it
	encoder := json.NewEncoder(w)
	err = encoder.Encode(&information)
	if err != nil {
		logger.Fatal(err.Error())
	}

}

/*
 * Description: Handles the GET method on the class/{classroom_id} route
 * Param: w Writer	r http.Request
 */
func handleGetClass(w http.ResponseWriter, r *http.Request) {
	classroom_id := mux.Vars(r)["classroom_id"]
	classroom := Classroom{}
	questions := []Question{}
	encoder := json.NewEncoder(w)

	//get the specified class by classroom_id
	err := getClass(&classroom, &questions, classroom_id)
	if err != nil {
		//if the class does not exist, display the 204 error
		information := Info{
			Code:        204,
			Description: codes[204],
		}
		err = encoder.Encode(&information)
	} else {

		//if there is no error, then encode the class and send it
		err = encoder.Encode(&classroom)
		if err != nil {
			logger.Fatal(err.Error())
		}
	}

}

/*
 * Description: Handles the GET method on the class route
 * Param: w Writer	r http.Request
 */
func handleGetClasses(w http.ResponseWriter, r *http.Request) {
	encoder := json.NewEncoder(w)
	classroom := []Classroom{}

	//get all of the classes
	err := getClasses(&classroom)
	if err != nil {
		//if there is an error, send a 400 error
		information := Info{
			Code:        400,
			Description: codes[400],
			Message:     "Could not get classes",
		}
		encoder.Encode(&information)
	} else {
		if len(classroom) < 1 {
			//if there were no classes returned, then send a 204 error
			information := Info{
				Code:        204,
				Description: codes[204],
				Message:     "No results found",
			}

			encoder.Encode(&information)
		} else {
			//if results were found, encode them in json and send them
			classrooms := struct {
				Classrooms []Classroom `json:"classrooms"`
				Count      int         `json:"count"`
			}{classroom, len(classroom)}
			encoder.Encode(&classrooms)
		}
	}
}

/*
 * Description: Handles the POST method on the class/{classroom_id}/question route
 * Param: w Writer	r http.Request
 */
func handlePostQuestion(w http.ResponseWriter, r *http.Request) {
	encoder := json.NewEncoder(w)
	var information Info

	var secret string
	question_text := r.FormValue("question_text")
	classroom_id := mux.Vars(r)["classroom_id"]

	//get the cookie containing a secret token if there is one
	cookies, err := r.Cookie("secret_token")
	if err != nil {
		fmt.Fprint(w, "Could not retrieve secret token")
	} else {
		//if there is, verify that the secret matches the secret of the class
		secret = cookies.Value
		if verifySecret(classroom_id, secret) {
			//if the secret matches, authorized to post a question
			err = postQuestion(question_text, classroom_id)
			if err != nil {
				//if question could not be posted, return 400 error
				information = Info{
					Code:        400,
					Description: codes[400],
					Message:     "Could not post question into classroom " + classroom_id,
				}
			} else {
				//if question posted successfully, return 201 error
				information = Info{
					Code:        201,
					Description: codes[201],
					Message:     "Question successfully added to classroom " + classroom_id,
				}
			}

		} else {
			//if the secret does not match, they are not authorized, so return 403 error
			information = Info{
				Code:        403,
				Description: codes[403],
				Message:     "You do not have permission to post questions into class " + classroom_id,
			}
		}
	}

	encoder.Encode(&information)

}

/*
 * Description: Handles the POST method on the question/{question_id}/answer route
 * Param: w Writer	r http.Request
 */
func handlePostAnswer(w http.ResponseWriter, r *http.Request) {
	var information Info
	encoder := json.NewEncoder(w)

	question_id, err := strconv.Atoi(mux.Vars(r)["question_id"])
	answer_text := r.FormValue("answer_text")

	//try to post the answer
	err = postAnswer(answer_text, question_id)
	if err != nil {
		//if the answer could not be posted, return a 400 error
		information = Info{
			Code:        400,
			Description: codes[400],
			Message:     "Could not post answer",
		}
	} else {
		//if the answer was posted, return a 201 error
		information = Info{
			Code:        201,
			Description: codes[201],
			Message:     "Answer successfully posted to question",
		}
	}

	encoder.Encode(&information)
}

/*
 * Description: Handles the GET method on the class/{classroom_id}/question route
 * Param: w Writer	r http.Request
 */
func handleGetQuestions(w http.ResponseWriter, r *http.Request) {
	var information Info
	encoder := json.NewEncoder(w)

	question := []Question{}
	classroom_id := mux.Vars(r)["classroom_id"]

	err := getQuestions(&question, classroom_id)
	if err != nil {
		information = Info{
			Code:        400,
			Description: codes[400],
		}

		encoder.Encode(&information)
	} else {
		if len(question) < 1 {
			information = Info{
				Code:        204,
				Description: codes[204],
				Message:     "No results found",
			}
			encoder.Encode(&information)
		} else {
			questions := struct {
				Questions []Question `json:"questions"`
				Count     int        `json:"count"`
			}{question, len(question)}
			encoder.Encode(&questions)
		}

	}

}

/*
 * Description: Handles the GET method on the question/{question_id}/answer route
 * Param: w Writer	r http.Request
 */
func handleGetAnswers(w http.ResponseWriter, r *http.Request) {
	var information Info
	encoder := json.NewEncoder(w)

	answer := []Answer{}
	question_id, err := strconv.Atoi(mux.Vars(r)["question_id"])

	//get the answers
	err = getAnswers(&answer, question_id)
	if err != nil {
		//if the answers couldn't be retrieved, return a 400 error
		information = Info{
			Code:        400,
			Description: codes[400],
			Message:     "Could not get answers for question " + strconv.Itoa(question_id),
		}

		encoder.Encode(&information)
	} else {
		//if they were retrieved, check count
		if len(answer) < 1 {
			//if there were no results, return a 204 error
			information = Info{
				Code:        204,
				Description: codes[204],
				Message:     "No results found for question " + strconv.Itoa(question_id),
			}
			encoder.Encode(&information)

		} else {
			//if there were results, encode them in json and send them
			answers := struct {
				Answers []Answer `json:"answers"`
				Count   int      `json:"count"`
			}{answer, len(answer)}
			encoder.Encode(&answers)
		}
	}
}

/*
 * Description: Handles the PUT method on the question/{question_id}/answer
 * Param: w Writer	r http.Request
 */
func handleSelectAnswer(w http.ResponseWriter, r *http.Request) {
	var information Info
	encoder := json.NewEncoder(w)

	var old_answer_id int
	expires := time.Now().Add(time.Minute)
	answer_id, err := strconv.Atoi(r.URL.Query().Get("answer_id"))
	if err != nil {
		logger.Fatal(err.Error())
	}

	oldCookie, err := r.Cookie("PrevAnswer")
	if err != nil {
		//if cookie does not exist, question was not answered recently
		//answer the question
		err = selectAnswer(answer_id)
		if err != nil {
			information = Info{
				Code:        400,
				Description: codes[400],
				Message:     "Could not select answer",
			}
		} else {
			information = Info{
				Code:        200,
				Description: codes[200],
				Message:     "Answer successfully selected",
			}
		}
	} else {
		//if cookie does exist, question was answered recently
		//change the selected answer
		old_answer_id, err = strconv.Atoi(oldCookie.Value)
		if err != nil {
			fmt.Fprint(w, err.Error())
		}
		err = deselectAnswer(old_answer_id)
		if err != nil {
			information = Info{
				Code:        400,
				Description: codes[400],
				Message:     "Previous answer could not be deselected",
			}
		} else {
			err = selectAnswer(answer_id)
			if err != nil {
				information = Info{
					Code:        400,
					Description: codes[400],
					Message:     "New answer could not be selected",
				}
			} else {
				information = Info{
					Code:        200,
					Description: codes[200],
					Message:     "Answer successfully changed",
				}
			}
		}

	}

	cookie := http.Cookie{Name: "PrevAnswer", Value: strconv.Itoa(answer_id), Expires: expires}
	http.SetCookie(w, &cookie)

	encoder.Encode(&information)

}

/*
 * Description: Handles the DELETE method on the question/{question_id} route
 * Param: w Writer	r http.Request
 */
func handleDeleteQuestion(w http.ResponseWriter, r *http.Request) {
	var information Info
	encoder := json.NewEncoder(w)

	question_id, err := strconv.Atoi(mux.Vars(r)["question_id"])
	if err != nil {
		logger.Fatal(err.Error())
	}

	//delete the question by ID
	err = deleteQuestion(question_id)
	if err != nil {
		//if the question could not be deleted, return 400 error
		information = Info{
			Code:        400,
			Description: codes[400],
			Message:     "Could not delete question " + strconv.Itoa(question_id),
		}
	} else {
		//if the question was deleted, return a 200 error
		information = Info{
			Code:        200,
			Description: codes[200],
			Message:     "Successfully deleted question " + strconv.Itoa(question_id),
		}
	}

	encoder.Encode(&information)

}

func main() {
	router := mux.NewRouter()

	router.HandleFunc("/api/v1/class", handlePostClass).Methods("POST")
	router.HandleFunc("/api/v1/class", handleGetClasses).Methods("GET")
	router.HandleFunc("/api/v1/class/{classroom_id}", handleGetClass).Methods("GET")
	router.HandleFunc("/api/v1/question/{question_id}", handleDeleteQuestion).Methods("DELETE")
	router.HandleFunc("/api/v1/class/{classroom_id}/question", handleGetQuestions).Methods("GET")
	router.HandleFunc("/api/v1/class/{classroom_id}/question", handlePostQuestion).Methods("POST")
	router.HandleFunc("/api/v1/question/{question_id}/answer", handleGetAnswers).Methods("GET")
	router.HandleFunc("/api/v1/question/{question_id}/answer", handlePostAnswer).Methods("POST")
	router.HandleFunc("/api/v1/question/{question_id}/answer", handleSelectAnswer).Methods("PUT")
	router.PathPrefix("/").Handler(http.FileServer(http.Dir("htdocs")))

	err := http.ListenAndServe(":8080", router)
	if err != nil {
		fmt.Errorf(err.Error())
	}

}
