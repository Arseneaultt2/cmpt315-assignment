Author: Tyler Arseneault
Student ID: 3052448
Email: arseneaultt2@mymacewan.ca
Class: CMPT 315
Instructor: Dr. Nick Boers

Notes:

5 files
assignment.go
database.go
setup.psql
Student Response System.pdf
README.txt

Database name: 
srs

Tables:
Classroom
Question
Answer

API was tested throughout development using Postman.
API documentation was initially created using Postman, but later copied into Word, where a PDF was created.

Known Deficiencies:

Anybody can delete a question from the database if they know the question ID, but only the creator of the class can add a question.

Instructions:

1. Run setup.psql in postgres

2. In database.go, change the global Connection variable

3. Run go build

4. Run the program
