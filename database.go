/*
 *	Author: Tyler Arseneault
 *	ID: 	3052448
 *	Due: 	Feb 6, 2018
 *	Class:	CMPT 315
 *			Assignment 1
 */

package main

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

/*
 * Simple struct to hold the information of the Classroom
 */
type Classroom struct {
	Classroom_ID string     `json:"classroomID" db:"classroom_id"`
	Name         string     `json:"name"        db:"name"`
	Questions    []Question `json:"questions"	db:"-"`
}

/*
 * Simple struct to hold the information of the Question
 */
type Question struct {
	Question_ID   int    `json:"questionID" db:"question_id"`
	Classroom_ID  string `json:"classroomID" db:"classroom_id"`
	Question_text string `json:"questionText" db:"question_text"`
}

/*
 * Simple struct to hold the information of the Answer
 */
type Answer struct {
	Answer_id   int    `json:"id" db:"answer_id"`
	Answer_text string `json:"answerText" db:"answer_text"`
	Question_ID int    `json:"questionID" db:"question_id"`
	Responses   int    `json:"responses" db:"responses"`
}

//Global variable for the database connection string
var Connection string = `dbname=srs user=postgres host=localhost port=5432
sslmode=disable`

/*
 * Description: Creates a connection to the database
 * Param:
 * Returns: *sqlx.DB, error
 */
func connectToDatabase() (*sqlx.DB, error) {
	return sqlx.Connect("postgres", Connection)
}

/*
 * Description: Creates a new classroom in the database
 * Param: classroom_id string, name string, secret string
 * Returns: error
 */
func postClass(classroom_id string, name string, secret string) error {
	db, err := connectToDatabase()
	if err != nil {
		return err
	}

	q := `INSERT INTO Classroom
		  (classroom_id, name, secret_token)
		  VALUES (:classroom_id, :name, :secret_token)`

	qArgs := struct {
		Classroom_ID string
		Name         string
		Secret_token string
	}{classroom_id, name, secret}

	_, err = db.NamedExec(q, qArgs)
	if err != nil {
		fmt.Errorf("NamedExec: " + err.Error())
		return err
	}

	return nil
}

/*
 * Description: Retrieves a specified class from the database
 * Param: classroom *Classroom, classroom_id string
 * Returns: error
 */
func getClass(classroom *Classroom, questions *[]Question, classroom_id string) error {
	db, err := connectToDatabase()
	if err != nil {
		return err
	}

	err = getQuestions(questions, classroom_id)
	if err != nil {

	} else {
		classroom.Questions = *questions
	}

	q := `SELECT classroom_id, name
		  FROM classroom
		  WHERE classroom_id LIKE ($1)`

	err = db.Get(classroom, q, classroom_id)
	if err != nil {
		return err
	}

	return nil
}

/*
 * Description: Retrieves all questions from a specific classroom id
 * Param: questions *[]Question, classroom_id string
 * Returns: error
 */
func getQuestions(questions *[]Question, classroom_id string) error {
	db, err := connectToDatabase()
	if err != nil {
		return err
	}

	q := `SELECT question_id, classroom_id, question_text
		  FROM Question
		  WHERE classroom_id LIKE ($1)
	  	  ORDER BY question_id ASC`

	err = db.Select(questions, q, classroom_id)
	if err != nil {
		return err
	}

	return nil
}

/*
 * Description: Retrieves a list of all classrooms
 * Param: classrooms *[]Classroom
 * Returns: error
 */
func getClasses(classrooms *[]Classroom) error {
	db, err := connectToDatabase()
	if err != nil {
		return err
	}

	q := `SELECT classroom_id, name
		  FROM Classroom
		  ORDER BY name ASC`

	err = db.Select(classrooms, q)
	if err != nil {
		return err
	}

	return nil
}

/*
 * Description: Creates a new question for a specifid classroom
 * Param: question_text string, classroom_id string
 * Returns: error
 */
func postQuestion(question_text string, classroom_id string) error {
	db, err := connectToDatabase()
	if err != nil {
		return err
	}

	q := `INSERT INTO Question (question_text, classroom_id)
		  VALUES (:question_text, :classroom_id)`

	qArgs := struct {
		Question_text string
		Classroom_id  string
	}{question_text, classroom_id}

	_, err = db.NamedExec(q, qArgs)
	if err != nil {
		return err
	}

	return nil

}

/*
 * Description: Creates a new answer for a specifid question
 * Param: answer_text string, question_id int
 * Returns: error
 */
func postAnswer(answer_text string, question_id int) error {
	db, err := connectToDatabase()
	if err != nil {
		return err
	}

	q := `INSERT INTO Answer (answer_text, question_id, responses)
		  VALUES (:answer_text, :question_id, :responses)`

	qArgs := struct {
		Answer_text string
		Question_ID int
		Responses   int
	}{answer_text, question_id, 0}

	_, err = db.NamedExec(q, qArgs)
	if err != nil {
		return err
	}

	return nil
}

/*
 * Description: Retrieves all of the answers for a specified question
 * Param: answers *[]Answer, question_id int
 * Returns: error
 */
func getAnswers(answers *[]Answer, question_id int) error {
	db, err := connectToDatabase()
	if err != nil {
		return err
	}

	q := `SELECT answer_id, answer_text, question_id, responses
		  FROM Answer
		  WHERE question_id = ($1)
		  ORDER BY answer_id ASC`

	err = db.Select(answers, q, question_id)
	if err != nil {
		return err
	}

	return nil
}

/*
 * Description: Increases the responses column of the specified answer
 * Param: answer_id int
 * Returns: error
 */
func selectAnswer(answer_id int) error {
	db, err := connectToDatabase()
	if err != nil {
		return err
	}

	q := `UPDATE Answer
		  SET responses = responses + 1
		  WHERE answer_id = :answer_id`

	qArgs := struct {
		Answer_id int
	}{answer_id}

	_, err = db.NamedExec(q, qArgs)
	if err != nil {
		return err
	}

	return nil

}

/*
 * Description: Decreases the responses column of the specified answer
 * Param: answer_id int
 * Returns: error
 */
func deselectAnswer(answer_id int) error {
	db, err := connectToDatabase()
	if err != nil {
		return err
	}

	q := `UPDATE Answer
		  SET responses = responses - 1
		  WHERE answer_id = :answer_id`

	qArgs := struct {
		Answer_id int
	}{answer_id}

	_, err = db.NamedExec(q, qArgs)
	if err != nil {
		return err
	}

	return nil
}

/*
 * Description: Verifies that the secret for the classroom is correct
 * Param: classroom_id string, secret_token string
 * Returns: bool
 */
func verifySecret(classroom_id string, secret_token string) bool {
	db, err := connectToDatabase()
	if err != nil {
		return false
	}

	var secretExists bool

	q := `SELECT classroom_id, secret_token
		  FROM Classroom
		  WHERE classroom_id = ($1) AND secret_token = ($2)`

	rows, err := db.Queryx(q, classroom_id, secret_token)
	if err != nil {
		return false
	}

	//if there was a result, then the secret matches
	//otherwise, it does not
	if rows.Next() {
		secretExists = true
	} else {
		secretExists = false
	}

	return secretExists
}

/*
 * Description: Deletes the answers that belong to a specified question, and then deletes that question
 * Param: question_id int
 * Returns: error
 */
func deleteQuestion(question_id int) error {
	db, err := connectToDatabase()
	if err != nil {
		return err
	}

	q := `DELETE FROM Answer
		  WHERE question_id = ($1)`

	_, err = db.Exec(q, question_id)
	if err != nil {
		return err
	}

	q = `DELETE FROM Question
		 WHERE question_id = ($1)`

	_, err = db.Exec(q, question_id)
	if err != nil {
		return err
	}

	return nil
}
