///<reference types="dot"/>
var Model = /** @class */ (function () {
    function Model(view) {
        var _this = this;
        this.getClasses = function () {
            var req = new XMLHttpRequest();
            req.addEventListener("load", function (evt) {
                _this.view.showClasses(JSON.parse(req.response));
                var joinBtns = document.getElementsByClassName("join-btn");
                for (var i = 0; i < joinBtns.length; i++)
                    joinBtns[i].addEventListener("click", _this.joinClass);
            });
            req.open("GET", "http://localhost:8080/api/v1/class");
            req.send();
        };
        this.submitClass = function (evt) {
            var id = document.querySelector("#classroom-id").value;
            var name = document.querySelector("#classroom-name").value;
            var req = new XMLHttpRequest();
            req.addEventListener("load", function (evt) {
                _this.getClasses();
            });
            req.open("POST", "http://localhost:8080/api/v1/class");
            req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            req.send("classroom_id=" + id + "&name=" + name);
        };
        this.joinClass = function (evt) {
            var id = evt.target.innerHTML;
            var req = new XMLHttpRequest();
            req.addEventListener("load", function (evt) {
                _this.view.showClass(JSON.parse(req.response));
                document.querySelector("#create-question-btn").addEventListener("click", function (evt) {
                    _this.createQuestion();
                });
            });
            req.open("GET", "http://localhost:8080/api/v1/class/" + id);
            req.send();
        };
        this.createQuestion = function () {
            var question = document.querySelector("#question").value;
            var answer1 = document.querySelector("#answer1").value;
            var answer2 = document.querySelector("#answer2").value;
            var answer3 = document.querySelector("#answer3").value;
            var answer4 = document.querySelector("#answer4").value;
            console.log(question, answer1, answer2, answer3, answer4);
        };
        window.addEventListener("load", function (evt) {
            _this.getClasses();
        });
        document.querySelector("#submit-class").addEventListener("click", this.submitClass);
        this.view = view;
    }
    return Model;
}());
var View = /** @class */ (function () {
    function View() {
        this.showClasses = function (classes) {
            var template = document.querySelector("#classes-temp").innerHTML;
            var func = doT.template(template);
            var rendered = func(classes);
            document.querySelector("#classes").innerHTML = rendered;
        };
        this.showClass = function (classObj) {
            var template = document.querySelector("#joined-class-temp").innerHTML;
            var func = doT.template(template);
            var rendered = func(classObj);
            document.querySelector("#joined-class").innerHTML = rendered;
        };
    }
    return View;
}());
var responseSystem = {};
responseSystem.view = new View();
responseSystem.model = new Model(responseSystem.view);
