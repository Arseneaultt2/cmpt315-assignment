///<reference types="dot"/>

/*
 *	Implementing the Model-View-Controller Design Pattern
 */

interface Controller {
	view: View;
	model: Model;
}

interface ClassObj {
    classroomID: string;
    name: string;
    questions: Array<QuestionObj>;
}

interface ClassList {
    classrooms: Array<ClassObj>;
    count: number;
}

interface QuestionObj {
	id: number;
	test: string;
}

class Model {
	private view: View;

	constructor(view: View){
		window.addEventListener("load", (evt: Event) => {
			this.getClasses();
		});
		document.querySelector("#submit-class").addEventListener("click", this.submitClass);
		this.view = view;
	}

	public getClasses = () => {
		let req = new XMLHttpRequest();
		req.addEventListener("load", (evt: Event) => {
			this.view.showClasses(JSON.parse(req.response) as ClassList);
			let  joinBtns = document.getElementsByClassName("join-btn");
			for(let i = 0; i < joinBtns.length; i++)
				joinBtns[i].addEventListener("click", this.joinClass);
		});
		req.open("GET", "http://localhost:8080/api/v1/class");
		req.send();
	}

	public submitClass = (evt: Event) => {
		let id = document.querySelector("#classroom-id").value;
		let name = document.querySelector("#classroom-name").value;
		let req = new XMLHttpRequest();
		req.addEventListener("load", (evt: Event) => {
			this.getClasses();
		});
		req.open("POST", "http://localhost:8080/api/v1/class");
		req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		req.send(`classroom_id=${id}&name=${name}`);
	}

	public joinClass = (evt: Event) => {
		let id = evt.target.innerHTML;
		let req = new XMLHttpRequest();
		req.addEventListener("load", (evt: Event) => {
			this.view.showClass(JSON.parse(req.response) as ClassObj);
			document.querySelector("#create-question-btn").addEventListener("click", (evt: Event) => {
				this.createQuestion();
			});
		});
		req.open("GET", `http://localhost:8080/api/v1/class/${id}`);
		req.send();
	}

	public createQuestion = () => {
		let question = document.querySelector("#question").value;
		let answer1 = document.querySelector("#answer1").value;
		let answer2 = document.querySelector("#answer2").value;
		let answer3 = document.querySelector("#answer3").value;
		let answer4 = document.querySelector("#answer4").value;

		console.log(question, answer1, answer2, answer3, answer4);
	}

}

class View {
	public showClasses = (classes) => {
		let template = document.querySelector("#classes-temp").innerHTML;
		let func = doT.template(template);
		let rendered = func(classes);
		document.querySelector("#classes").innerHTML = rendered;
	}

	public showClass = (classObj) => {
		let template = document.querySelector("#joined-class-temp").innerHTML;
		let func = doT.template(template);
		let rendered = func(classObj);
		document.querySelector("#joined-class").innerHTML = rendered;
	}
}

let responseSystem: Controller = {} as Controller;
responseSystem.view = new View();
responseSystem.model = new Model(responseSystem.view);